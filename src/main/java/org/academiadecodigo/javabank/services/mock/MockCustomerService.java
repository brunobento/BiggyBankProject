package org.academiadecodigo.javabank.services.mock;

import org.academiadecodigo.javabank.model.Customer;
import org.academiadecodigo.javabank.model.Recipient;
import org.academiadecodigo.javabank.model.account.Account;
import org.academiadecodigo.javabank.services.CustomerService;

import java.util.ArrayList;
import java.util.List;

public class MockCustomerService extends AbstractMockService<Customer> implements CustomerService {

    @Override
    public Customer get(Integer id) {
        return modelMap.get(id);
    }

    @Override
    public double getBalance(Integer customerId) {

        List<Account> accounts = modelMap.get(customerId).getAccounts();

        double balance = 0;
        for (Account account : accounts) {
            balance += account.getBalance();
        }

        return balance;

    }

    @Override
    public List<Customer> list() {
        return new ArrayList<>(modelMap.values());
    }

    @Override
    public void delete(Integer id) {
        modelMap.remove(id);
    }

    @Override
    public List<Recipient> listRecipients(Integer id) {
        return modelMap.get(id).getRecipients();
    }

    @Override
    public void removeRecipient(Integer id, Integer recipientId) {

        Customer customer = modelMap.get(id);
        Recipient recipient = null;

        for (Recipient rcpt : customer.getRecipients()) {
            if (rcpt.getId().equals(recipientId)) {
                recipient = rcpt;
            }
        }

        if (recipient != null) {
            customer.removeRecipient(recipient);
        }
    }

    @Override
    public Customer save(Customer customer) {
        return null;
    }
}
