package org.academiadecodigo.javabank.persistence.dao;

import org.academiadecodigo.javabank.model.Recipient;

public interface RecipientDao extends Dao<Recipient> {
}
