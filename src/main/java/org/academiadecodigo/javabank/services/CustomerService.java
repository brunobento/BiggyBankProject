package org.academiadecodigo.javabank.services;

import org.academiadecodigo.javabank.model.Customer;
import org.academiadecodigo.javabank.model.Recipient;

import java.util.List;

public interface CustomerService {

    List<Customer> list();

    Customer get(Integer id);

    void delete(Integer id);

    double getBalance(Integer id);

    List<Recipient> listRecipients(Integer id);

    void removeRecipient(Integer id, Integer recipientId);

    Customer save(Customer customer);
}
