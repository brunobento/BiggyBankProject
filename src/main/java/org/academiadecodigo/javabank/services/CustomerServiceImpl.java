package org.academiadecodigo.javabank.services;

import org.academiadecodigo.javabank.model.Customer;
import org.academiadecodigo.javabank.model.Recipient;
import org.academiadecodigo.javabank.model.account.Account;
import org.academiadecodigo.javabank.persistence.dao.AccountDao;
import org.academiadecodigo.javabank.persistence.dao.CustomerDao;
import org.academiadecodigo.javabank.persistence.dao.RecipientDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.academiadecodigo.javabank.errors.ErrorMessage.*;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerDao customerDao;
    private RecipientDao recipientDao;
    private AccountDao accountDao;

    @Autowired
    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Autowired
    public void setCustomerDao(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @Autowired
    public void setRecipientDao(RecipientDao recipientDao) {
        this.recipientDao = recipientDao;
    }

    @Override
    public Customer get(Integer id) {
        return customerDao.findById(id);
    }

    @Override
    public double getBalance(Integer id) {

        Customer customer = customerDao.findById(id);

        if (customer == null) {
            throw new IllegalArgumentException(CUSTOMER_NOT_FOUND);
        }

        List<Account> accounts = customer.getAccounts();

        double balance = 0;
        for (Account account : accounts) {
            balance += account.getBalance();
        }

        return balance;
    }

    @Override
    public List<Customer> list() {
        return customerDao.findAll();
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        customerDao.delete(id);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Recipient> listRecipients(Integer id) {

        // check then act logic requires transaction,
        // event if read only
        Customer customer = customerDao.findById(id);

        if (customer == null) {
            throw new IllegalArgumentException(CUSTOMER_NOT_FOUND);
        }

        return new ArrayList<>(customerDao.findById(id).getRecipients());
    }

    @Transactional
    @Override
    public void removeRecipient(Integer id, Integer recipientId) {

        Customer customer = customerDao.findById(id);

        Recipient recipient = recipientDao.findById(recipientId);

        if (customer == null) {
            throw new IllegalArgumentException(CUSTOMER_NOT_FOUND);
        }

        if (recipient == null) {
            throw new IllegalArgumentException(RECIPIENT_NOT_FOUND);
        }

        if (!recipient.getCustomer().getId().equals(id)) {
            throw new IllegalArgumentException(CUSTOMER_RECIPIENT_NOT_FOUND);
        }

        customer.removeRecipient(recipient);
        customerDao.saveOrUpdate(customer);
    }

    @Override
    @Transactional
    public Customer save(Customer customer){
        return customerDao.saveOrUpdate(customer);
    }
}
